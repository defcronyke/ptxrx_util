pub mod conf {
    pub const N_CLIENTS: usize = 2; //set to desired number
    pub const SERVER: &str = "ws://127.0.0.1:3000/ws";
}
